CREATE OR REPLACE PROCEDURE public.pr_crear_actualiza_ruleta(
	p_token character varying,
	p_id_usuario integer,
	p_id_estado integer,
	p_numganador integer,
	INOUT p_id_ruleta integer,
	INOUT p_message_out character varying,
	INOUT p_eserror_out boolean)
   -- RETURNS record
    LANGUAGE 'plpgsql'

   -- COST 100
   -- VOLATILE 
    
AS $BODY$DECLARE
  --V_CURSOR refcursor;                                   
  -- Declare a cursor variable
  CSCRULETA INTEGER:= 0;
BEGIN

	CSCRULETA:=(SELECT (COALESCE(MAX(ID_RULETA),0)+1) FROM SOL_RULETA);
	
	IF P_ID_RULETA=0 THEN
	
		IF EXISTS(SELECT ID_TOKEN FROM SOL_TOKEN WHERE TOKEN=P_TOKEN AND ID_USUARIO=P_ID_USUARIO AND VIGENTE=TRUE) THEN
			
			P_ESERROR_OUT := FALSE;
			P_MESSAGE_OUT := 'Se creó correctamente la ruleta.';
	
			WITH UPD AS (
			INSERT INTO SOL_RULETA
						(ID_RULETA
					   , DESCRIPCION
					   , ESTADO
					   , VIGENTE
					   , USU_CREA
					   , FEC_CREA)
			 VALUES
				   (CSCRULETA,
				   'Ruleta'||CSCRULETA,
					P_ID_ESTADO,
					TRUE,
					USER,
					NOW()) RETURNING ID_RULETA)
			SELECT INTO P_ID_RULETA ID_RULETA FROM UPD;
			
			IF P_ID_RULETA IS NULL THEN	
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'No fue posible generar ruleta.';	
	 		
			END IF;
				
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'Autenticación errada.';		 
			
		END IF;		
		
	ELSE
		
		P_ESERROR_OUT := FALSE;
		IF P_ID_ESTADO=2 THEN
			P_MESSAGE_OUT := 'La operación de apertura de la ruleta fue exitoso.';
		ELSE
			P_MESSAGE_OUT := 'La cierre de la ruleta fue exitoso.';
		END IF;
	
		UPDATE PUBLIC.SOL_RULETA
		SET ESTADO=P_ID_ESTADO, 
		NUMGANADOR=P_NUMGANADOR, 
		USU_MODIF=USER, 
		FEC_MODIF=NOW()
		WHERE ID_RULETA=P_ID_RULETA;
	
	END IF;
	
END$BODY$;

ALTER PROCEDURE public.pr_crear_actualiza_ruleta(character varying, integer,integer, integer, integer,character varying,  boolean)
    OWNER TO postgres;