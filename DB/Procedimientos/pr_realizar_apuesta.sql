CREATE OR REPLACE PROCEDURE public.pr_registrar_log(
	p_msjerror character varying,
	p_codigoerror integer,
	p_metodo character varying,
	INOUT p_message_out character varying,
	INOUT p_eserror_out boolean)
    LANGUAGE 'plpgsql'
  
AS $BODY$DECLARE

BEGIN
		
	P_ESERROR_OUT := FALSE;
	P_MESSAGE_OUT := 'Se creó correctamente el log.';
	
	INSERT INTO PUBLIC.TA_AUDITORIA(
	 CODIGOERROR, MSJERROR, METODO, VIGENTE, USU_CREA, FEC_CREA)
	VALUES ( P_CODIGOERROR, P_MSJERROR, P_METODO, TRUE,USER,NOW());
			
END$BODY$;

ALTER PROCEDURE public.pr_registrar_log(character varying, integer,character varying,character varying,boolean)
    OWNER TO postgres;