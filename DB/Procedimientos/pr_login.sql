-- PROCEDURE: public.pr_login(character varying, character varying, integer, character varying, character varying, boolean)

-- DROP PROCEDURE public.pr_login(character varying, character varying, integer, character varying, character varying, boolean);

CREATE OR REPLACE PROCEDURE public.pr_login(
	p_user character varying,
	p_pass character varying,
	INOUT p_id_usuario integer,
	INOUT p_token character varying,
	INOUT p_message_out character varying,
	INOUT p_eserror_out boolean)
LANGUAGE 'plpgsql'

AS $BODY$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Inicio de sesion exitoso.';
	P_ID_USUARIO:=(SELECT ID_USUARIO FROM SOL_USUARIO WHERE USUARIO = P_USER AND CONTRASENA=P_PASS AND VIGENTE=TRUE);
	
  IF (P_ID_USUARIO>0) THEN
  
	
		P_TOKEN:=(SELECT FLOOR(RANDOM()* (11) + 5)||''|| TO_CHAR(NOW() , 'YYYYMMDDHH24MISSMS')  ||''||FLOOR(RANDOM()* (11) + 5));
			 
		IF (P_TOKEN IS NOT NULL)THEN
		
			INSERT INTO PUBLIC.SOL_TOKEN(TOKEN, ID_USUARIO, VIGENTE, USU_CREA, FEC_CREA)
			VALUES (P_TOKEN, P_ID_USUARIO, TRUE, USER, NOW());
					
			
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'Error al generar token.';
		
		END IF; 
				
	ELSE
	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Usuario o contraseña incorrectos, por favor revisar'; 
		P_TOKEN:=NULL;
		
	END IF;
		
	--RETURN 0;
	
END$BODY$;
ALTER PROCEDURE public.pr_login(character varying, character varying,integer,character varying,character varying,  boolean)
    OWNER TO postgres;
