-- PROCEDURE: public.pr_logout( integer, character varying, character varying, boolean)

-- DROP PROCEDURE public.pr_logout( integer, character varying, character varying, boolean);

CREATE OR REPLACE PROCEDURE public.pr_logout(
	p_id_usuario integer,
	p_token character varying,
	INOUT p_message_out character varying,
	INOUT p_eserror_out boolean)
LANGUAGE 'plpgsql'

AS $BODY$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Cierre de sesion exitoso.';
	
	UPDATE PUBLIC.SOL_TOKEN
	SET VIGENTE=FALSE
	WHERE ID_TOKEN=P_TOKEN AND ID_USUARIO=P_ID_USUARIO;
	
END$BODY$;
ALTER PROCEDURE public.pr_logout(integer,character varying,character varying,  boolean)
    OWNER TO postgres;
