CREATE OR REPLACE FUNCTION public.fr_listar_ruleta(
	p_token character varying,
	p_id_usuario integer)
	RETURNS TABLE(ID_RULETA integer,NOMBRERULETA character varying, ID_ESTADO integer,DESCRIPCION character varying) 
	AS $$
	DECLARE
	
  P_NUMGANADOR INTEGER:= 0;  
  P_ESPAR INTEGER:= 0;  
  -- Declare a cursor variable
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY  SELECT SR.ID_RULETA,SR.DESCRIPCION AS NOMBRERULETA,ME.ID_ESTADO,ME.DESCRIPCION 
		FROM  sol_ruleta AS SR INNER JOIN ma_estados AS ME ON ME.ID_ESTADO=SR.ESTADO;
	
	END IF;
	
END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION public.fr_listar_ruleta(character varying, integer)
	OWNER TO postgres;
