PGDMP     6    /        	    
    x            Masivian    12.2    12.2 ?    s           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            t           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            u           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            v           1262    17468    Masivian    DATABASE     �   CREATE DATABASE "Masivian" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE "Masivian";
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            w           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1255    17619 >   fr_cerrar_ruleta(character varying, integer, integer, integer)    FUNCTION     \  CREATE FUNCTION public.fr_cerrar_ruleta(p_token character varying, p_id_usuario integer, p_id_estado integer, p_id_ruleta integer) RETURNS TABLE(id_usuario integer, nombre character varying, id_ruleta integer, nombre_ruleta character varying, numganador integer, numero_apostado integer, apuesta double precision, color_apostado character varying, valorganado double precision, ganador text)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
  P_NUMGANADOR INTEGER:= 0;  
  P_ESPAR INTEGER:= 0;  
  -- Declare a cursor variable
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		IF NOT EXISTS(SELECT SOLR.NUMGANADOR FROM PUBLIC.SOL_RULETA SOLR WHERE SOLR.ID_RULETA=P_ID_RULETA)THEN
		WITH UPD AS (UPDATE PUBLIC.SOL_RULETA 
		SET ESTADO=P_ID_ESTADO, 
		NUMGANADOR=(SELECT FLOOR(RANDOM()* (36-0+1) + 0)), 
		USU_MODIF=USER, 
		FEC_MODIF=NOW()
		WHERE PUBLIC.SOL_RULETA.ID_RULETA=P_ID_RULETA RETURNING PUBLIC.SOL_RULETA.NUMGANADOR)
		SELECT INTO P_NUMGANADOR UPD.NUMGANADOR FROM UPD;
		
		END IF;		
		P_ESPAR:=(select mod(P_NUMGANADOR,2));
		
		IF P_ESPAR=0 THEN
		
			RETURN QUERY SELECT SA.ID_USUARIO,SU.NOMBRE, SR.ID_RULETA,SR.DESCRIPCION AS NOMBRE_RULETA,
								SR.NUMGANADOR,SA.APUESTA_NUMERO AS NUMERO_APOSTADO, SA.APUESTA,COALESCE(MC.DESCRIPCION,'') AS COLOR_APOSTADO,
								CASE WHEN SA.APUESTA_NUMERO=P_NUMGANADOR AND SA.APUESTA_NUMERO>0 THEN SA.APUESTA*5
										WHEN  MOD(SA.APUESTA_NUMERO,2)=0 THEN SA.APUESTA*1.8 
										ELSE 0
										END AS VALORGANADO,
								CASE WHEN SA.APUESTA_NUMERO=P_NUMGANADOR OR MOD(SA.APUESTA_NUMERO,2)=0 THEN 'GANADOR'
										ELSE 'PERDEDOR'
										END AS GANADOR
								FROM SOL_APUESTA SA INNER JOIN SOL_RULETA SR ON SA.ID_RULETA=SR.ID_RULETA
								INNER JOIN SOL_USUARIO SU ON SU.ID_USUARIO=SA.ID_USUARIO
								LEFT JOIN MA_COLOR MC ON MC.ID_COLOR=SA.APUESTA_COLOR
								WHERE (P_ID_RULETA IS NULL OR SA.ID_RULETA = P_ID_RULETA) ;   -- OPEN A CURSOR
						  
		ELSE
			
			RETURN QUERY SELECT SA.ID_USUARIO,SU.NOMBRE, SR.ID_RULETA,SR.DESCRIPCION AS NOMBRE_RULETA,
								SR.NUMGANADOR,SA.APUESTA_NUMERO ASNUMERO_APOSTADO, SA.APUESTA,COALESCE(MC.DESCRIPCION,'') AS COLOR_APOSTADO,
								CASE WHEN SA.APUESTA_NUMERO=P_NUMGANADOR AND SA.APUESTA_NUMERO>0 THEN SA.APUESTA*5
										WHEN  MOD(SA.APUESTA_NUMERO,2)>0 THEN SA.APUESTA*1.8 
										ELSE 0
										END AS VALORGANADO,
								CASE WHEN SA.APUESTA_NUMERO=P_NUMGANADOR OR MOD(SA.APUESTA_NUMERO,2)>0 THEN 'GANADOR'
										ELSE 'PERDEDOR'
										END AS GANADOR
								FROM SOL_APUESTA SA INNER JOIN SOL_RULETA SR ON SA.ID_RULETA=SR.ID_RULETA
								INNER JOIN SOL_USUARIO SU ON SU.ID_USUARIO=SA.ID_USUARIO
								LEFT JOIN MA_COLOR MC ON MC.ID_COLOR=SA.APUESTA_COLOR
								WHERE (P_ID_RULETA IS NULL OR SA.ID_RULETA = P_ID_RULETA) ;   -- OPEN A CURSOR		
		END IF;
	
	END IF;
	
	END;
$$;
 �   DROP FUNCTION public.fr_cerrar_ruleta(p_token character varying, p_id_usuario integer, p_id_estado integer, p_id_ruleta integer);
       public          postgres    false    3            �            1255    17623 ,   fr_listar_ruleta(character varying, integer)    FUNCTION     �  CREATE FUNCTION public.fr_listar_ruleta(p_token character varying, p_id_usuario integer) RETURNS TABLE(id_ruleta integer, nombreruleta character varying, id_estado integer, descripcion character varying)
    LANGUAGE plpgsql
    AS $$
	DECLARE
	
  P_NUMGANADOR INTEGER:= 0;  
  P_ESPAR INTEGER:= 0;  
  -- Declare a cursor variable
BEGIN

	IF EXISTS(SELECT ST.ID_TOKEN FROM SOL_TOKEN ST WHERE ST.TOKEN=P_TOKEN AND ST.ID_USUARIO=P_ID_USUARIO AND ST.VIGENTE=TRUE) THEN
		
		RETURN QUERY  SELECT SR.ID_RULETA,SR.DESCRIPCION AS NOMBRERULETA,ME.ID_ESTADO,ME.DESCRIPCION 
		FROM  sol_ruleta AS SR INNER JOIN ma_estados AS ME ON ME.ID_ESTADO=SR.ESTADO;
	
	END IF;
	
END;
$$;
 X   DROP FUNCTION public.fr_listar_ruleta(p_token character varying, p_id_usuario integer);
       public          postgres    false    3            �            1255    17602 l   pr_crear_actualiza_ruleta(character varying, integer, integer, integer, integer, character varying, boolean) 	   PROCEDURE     w  CREATE PROCEDURE public.pr_crear_actualiza_ruleta(p_token character varying, p_id_usuario integer, p_id_estado integer, p_numganador integer, INOUT p_id_ruleta integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                   
  -- Declare a cursor variable
  CSCRULETA INTEGER:= 0;
BEGIN

	CSCRULETA:=(SELECT (COALESCE(MAX(ID_RULETA),0)+1) FROM SOL_RULETA);
	
	IF P_ID_RULETA=0 THEN
	
		IF EXISTS(SELECT ID_TOKEN FROM SOL_TOKEN WHERE TOKEN=P_TOKEN AND ID_USUARIO=P_ID_USUARIO AND VIGENTE=TRUE) THEN
			
			P_ESERROR_OUT := FALSE;
			P_MESSAGE_OUT := 'Se creó correctamente la ruleta.';
	
			WITH UPD AS (
			INSERT INTO SOL_RULETA
						(ID_RULETA
					   , DESCRIPCION
					   , ESTADO
					   , VIGENTE
					   , USU_CREA
					   , FEC_CREA)
			 VALUES
				   (CSCRULETA,
				   'Ruleta'||CSCRULETA,
					P_ID_ESTADO,
					TRUE,
					USER,
					NOW()) RETURNING ID_RULETA)
			SELECT INTO P_ID_RULETA ID_RULETA FROM UPD;
			
			IF P_ID_RULETA IS NULL THEN	
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'No fue posible generar ruleta.';	
	 		
			END IF;
				
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'Autenticación errada.';		 
			
		END IF;		
		
	ELSE
		
		P_ESERROR_OUT := FALSE;
		IF P_ID_ESTADO=2 THEN
			P_MESSAGE_OUT := 'La operación de apertura de la ruleta fue exitoso.';
		ELSE
			P_MESSAGE_OUT := 'La cierre de la ruleta fue exitoso.';
		END IF;
	
		UPDATE PUBLIC.SOL_RULETA
		SET ESTADO=P_ID_ESTADO, 
		NUMGANADOR=P_NUMGANADOR, 
		USU_MODIF=USER, 
		FEC_MODIF=NOW()
		WHERE ID_RULETA=P_ID_RULETA;
	
	END IF;
	
END$$;
 �   DROP PROCEDURE public.pr_crear_actualiza_ruleta(p_token character varying, p_id_usuario integer, p_id_estado integer, p_numganador integer, INOUT p_id_ruleta integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    17564 f   pr_login(character varying, character varying, integer, character varying, character varying, boolean) 	   PROCEDURE     z  CREATE PROCEDURE public.pr_login(p_user character varying, p_pass character varying, INOUT p_id_usuario integer, INOUT p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Inicio de sesion exitoso.';
	P_ID_USUARIO:=(SELECT ID_USUARIO FROM SOL_USUARIO WHERE USUARIO = P_USER AND CONTRASENA=P_PASS AND VIGENTE=TRUE);
	
  IF (P_ID_USUARIO>0) THEN
  
		P_TOKEN:=(SELECT FLOOR(RANDOM()* (11) + 5)||''|| TO_CHAR(NOW() , 'YYYYMMDDHH24MISSMS')  ||''||FLOOR(RANDOM()* (11) + 5));
			 
		IF (P_TOKEN IS NOT NULL)THEN
		
			INSERT INTO PUBLIC.SOL_TOKEN(TOKEN, ID_USUARIO, VIGENTE, USU_CREA, FEC_CREA)
			VALUES (P_TOKEN, P_ID_USUARIO, TRUE, USER, NOW());
					
			
		ELSE
		
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'Error al generar token.';
		
		END IF;        	
		
	ELSE
	
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Usuario o contraseña incorrectos, por favor revisar'; 
		P_TOKEN:=NULL;
		
	END IF;
		
	--RETURN 0;
	
END$$;
 �   DROP PROCEDURE public.pr_login(p_user character varying, p_pass character varying, INOUT p_id_usuario integer, INOUT p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    17592 A   pr_logout(integer, character varying, character varying, boolean) 	   PROCEDURE     �  CREATE PROCEDURE public.pr_logout(p_id_usuario integer, p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                
  -- Declare a cursor variable  
BEGIN
	
	P_ESERROR_OUT := false;
	P_MESSAGE_OUT := 'Cierre de sesion exitoso.';
	
	UPDATE PUBLIC.SOL_TOKEN
	SET VIGENTE=FALSE
	WHERE ID_TOKEN=P_TOKEN AND ID_USUARIO=P_ID_USUARIO;
	
END$$;
 �   DROP PROCEDURE public.pr_logout(p_id_usuario integer, p_token character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    17605 �   pr_realizar_apuesta(character varying, integer, integer, integer, double precision, integer, integer, character varying, boolean) 	   PROCEDURE     o
  CREATE PROCEDURE public.pr_realizar_apuesta(p_token character varying, p_id_usuario integer, p_numero integer, p_color integer, p_valor double precision, p_id_ruleta integer, p_tipo_apuesta integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE
  --V_CURSOR refcursor;                                   
  -- Declare a cursor variable
  VALIDO BOOLEAN:= FALSE;
BEGIN
		
	IF EXISTS(SELECT ID_TOKEN FROM SOL_TOKEN WHERE TOKEN=P_TOKEN AND ID_USUARIO=P_ID_USUARIO AND VIGENTE=TRUE) THEN

		IF EXISTS(SELECT SC.VALOR FROM SOL_CREDITO SC INNER JOIN SOL_USUARIO SU ON SC.ID_USUARIO=SU.ID_USUARIO 	
		WHERE SC.ID_USUARIO=P_ID_USUARIO AND SC.VALOR>0 AND P_VALOR<=SC.VALOR) THEN
			
			IF EXISTS(SELECT SR.ID_RULETA FROM PUBLIC.SOL_RULETA SR INNER JOIN MA_ESTADOS ME ON ME.ID_ESTADO=SR.ESTADO AND SR.ID_RULETA=P_ID_RULETA
			WHERE SR.ESTADO=2 AND SR.VIGENTE=TRUE) THEN
			
				IF EXISTS(SELECT P_TIPO_APUESTA=(SELECT ID_TIPO FROM MA_TIPO_APUESTA WHERE ID_TIPO=2) ) THEN
				
					IF EXISTS(SELECT  ID_RANGO FROM MA_RANGO_NUMERO WHERE P_NUMERO >= RANGO_INI OR P_NUMERO<= RANGO_FIN)THEN
		
						INSERT INTO PUBLIC.SOL_APUESTA(
						APUESTA, ID_USUARIO, ID_RULETA, ID_TIPO, APUESTA_NUMERO, VIGENTE, USU_CREA, FEC_CREA)
						VALUES (P_VALOR, P_ID_USUARIO, P_ID_RULETA, P_TIPO_APUESTA, P_NUMERO, TRUE, USER, NOW());
						
						VALIDO:=TRUE;
						
					ELSE
					
						P_ESERROR_OUT := TRUE;
						P_MESSAGE_OUT := 'El número apostado no se encuentra dentro de los permitidos, los cuales son entre 0 y 36.';	
					
					END IF;
				
				ELSIF EXISTS(SELECT ID_TIPO FROM MA_TIPO_APUESTA WHERE ID_TIPO=P_TIPO_APUESTA) THEN 
						
					INSERT INTO PUBLIC.SOL_APUESTA(
					APUESTA, ID_USUARIO, ID_RULETA, ID_TIPO, APUESTA_COLOR, VIGENTE, USU_CREA, FEC_CREA)
					VALUES (P_VALOR, P_ID_USUARIO, P_ID_RULETA, P_TIPO_APUESTA, P_COLOR, TRUE, USER, NOW());
					VALIDO:=TRUE;
				
				ELSE
					
					P_ESERROR_OUT := TRUE;
					P_MESSAGE_OUT := 'Tipo de apuesta no identificado.';	
					
				END IF;
				
			ELSE
					
					P_ESERROR_OUT := TRUE;
					P_MESSAGE_OUT := 'Ruleta no se encuentra en estado abierto.';	
					
			END IF;
				
		ELSE
			
			P_ESERROR_OUT := TRUE;
			P_MESSAGE_OUT := 'Usuario no posee credito suficiente.';		 
			
		END IF;
	ELSE
		
		P_ESERROR_OUT := TRUE;
		P_MESSAGE_OUT := 'Credencial invalida.';		 
		
	END IF;	
	
	
	IF VALIDO THEN
	
		P_ESERROR_OUT := FALSE;
		P_MESSAGE_OUT := 'Se realizó la apuesta con exito.';	
		UPDATE PUBLIC.SOL_CREDITO
		SET VALOR=(SELECT (SC.VALOR-P_VALOR) FROM SOL_CREDITO SC WHERE SC.ID_USUARIO=P_ID_USUARIO)
		WHERE ID_USUARIO=P_ID_USUARIO;
	
	END IF;
			
END$$;
 
  DROP PROCEDURE public.pr_realizar_apuesta(p_token character varying, p_id_usuario integer, p_numero integer, p_color integer, p_valor double precision, p_id_ruleta integer, p_tipo_apuesta integer, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1255    17626 [   pr_registrar_log(character varying, integer, character varying, character varying, boolean) 	   PROCEDURE     �  CREATE PROCEDURE public.pr_registrar_log(p_msjerror character varying, p_codigoerror integer, p_metodo character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean)
    LANGUAGE plpgsql
    AS $$DECLARE

BEGIN
		
	P_ESERROR_OUT := FALSE;
	P_MESSAGE_OUT := 'Se creó correctamente el log.';
	
	INSERT INTO PUBLIC.TA_AUDITORIA(
	 CODIGOERROR, MSJERROR, METODO, VIGENTE, USU_CREA, FEC_CREA)
	VALUES ( P_CODIGOERROR, P_MSJERROR, P_METODO, TRUE,USER,NOW());
			
END$$;
 �   DROP PROCEDURE public.pr_registrar_log(p_msjerror character varying, p_codigoerror integer, p_metodo character varying, INOUT p_message_out character varying, INOUT p_eserror_out boolean);
       public          postgres    false    3            �            1259    17472    ma_color_id_color_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_color_id_color_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 ,   DROP SEQUENCE public.ma_color_id_color_seq;
       public          postgres    false    3            �            1259    17490    ma_color    TABLE     &  CREATE TABLE public.ma_color (
    id_color integer DEFAULT nextval('public.ma_color_id_color_seq'::regclass) NOT NULL,
    descripcion character varying(500) NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.ma_color;
       public         heap    postgres    false    202    3            �            1259    17474    ma_estados_id_estado_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_estados_id_estado_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 /   DROP SEQUENCE public.ma_estados_id_estado_seq;
       public          postgres    false    3            �            1259    17499 
   ma_estados    TABLE     ,  CREATE TABLE public.ma_estados (
    id_estado integer DEFAULT nextval('public.ma_estados_id_estado_seq'::regclass) NOT NULL,
    descripcion character varying(500) NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.ma_estados;
       public         heap    postgres    false    203    3            �            1259    17476    ma_rango_numero_id_rango_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_rango_numero_id_rango_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 3   DROP SEQUENCE public.ma_rango_numero_id_rango_seq;
       public          postgres    false    3            �            1259    17508    ma_rango_numero    TABLE     C  CREATE TABLE public.ma_rango_numero (
    id_rango integer DEFAULT nextval('public.ma_rango_numero_id_rango_seq'::regclass) NOT NULL,
    rango_ini integer NOT NULL,
    rango_fin integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
 #   DROP TABLE public.ma_rango_numero;
       public         heap    postgres    false    204    3            �            1259    17478    ma_tipo_apuesta_id_tipo_seq    SEQUENCE     �   CREATE SEQUENCE public.ma_tipo_apuesta_id_tipo_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 2   DROP SEQUENCE public.ma_tipo_apuesta_id_tipo_seq;
       public          postgres    false    3            �            1259    17514    ma_tipo_apuesta    TABLE     2  CREATE TABLE public.ma_tipo_apuesta (
    id_tipo integer DEFAULT nextval('public.ma_tipo_apuesta_id_tipo_seq'::regclass) NOT NULL,
    descripcion character varying(500) NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
 #   DROP TABLE public.ma_tipo_apuesta;
       public         heap    postgres    false    205    3            �            1259    17480    sol_apuesta_id_apuesta_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_apuesta_id_apuesta_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 1   DROP SEQUENCE public.sol_apuesta_id_apuesta_seq;
       public          postgres    false    3            �            1259    17606    sol_apuesta    TABLE     �  CREATE TABLE public.sol_apuesta (
    id_apuesta integer DEFAULT nextval('public.sol_apuesta_id_apuesta_seq'::regclass) NOT NULL,
    apuesta double precision NOT NULL,
    id_usuario integer NOT NULL,
    id_ruleta integer NOT NULL,
    id_tipo integer NOT NULL,
    apuesta_numero integer,
    apuesta_color integer,
    vigente boolean NOT NULL,
    usu_crea character varying(10),
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.sol_apuesta;
       public         heap    postgres    false    206    3            �            1259    17482    sol_credito_id_credito_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_credito_id_credito_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 1   DROP SEQUENCE public.sol_credito_id_credito_seq;
       public          postgres    false    3            �            1259    17529    sol_credito    TABLE     E  CREATE TABLE public.sol_credito (
    id_credito integer DEFAULT nextval('public.sol_credito_id_credito_seq'::regclass) NOT NULL,
    valor double precision NOT NULL,
    id_usuario integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.sol_credito;
       public         heap    postgres    false    207    3            �            1259    17484    sol_ruleta_id_ruleta_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_ruleta_id_ruleta_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 /   DROP SEQUENCE public.sol_ruleta_id_ruleta_seq;
       public          postgres    false    3            �            1259    17593 
   sol_ruleta    TABLE     �  CREATE TABLE public.sol_ruleta (
    id_ruleta integer DEFAULT nextval('public.sol_ruleta_id_ruleta_seq'::regclass) NOT NULL,
    descripcion character varying(500) NOT NULL,
    estado integer NOT NULL,
    numganador integer,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    usu_modif character varying(50),
    fec_modif timestamp with time zone
);
    DROP TABLE public.sol_ruleta;
       public         heap    postgres    false    208    3            �            1259    17486    sol_token_id_token_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_token_id_token_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 -   DROP SEQUENCE public.sol_token_id_token_seq;
       public          postgres    false    3            �            1259    17544 	   sol_token    TABLE     C  CREATE TABLE public.sol_token (
    id_token integer DEFAULT nextval('public.sol_token_id_token_seq'::regclass) NOT NULL,
    token character varying(100) NOT NULL,
    id_usuario integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp with time zone NOT NULL
);
    DROP TABLE public.sol_token;
       public         heap    postgres    false    209    3            �            1259    17488    sol_usuario_id_usuario_seq    SEQUENCE     �   CREATE SEQUENCE public.sol_usuario_id_usuario_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 1   DROP SEQUENCE public.sol_usuario_id_usuario_seq;
       public          postgres    false    3            �            1259    17550    sol_usuario    TABLE     �  CREATE TABLE public.sol_usuario (
    id_usuario integer DEFAULT nextval('public.sol_usuario_id_usuario_seq'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    usuario character varying(20) NOT NULL,
    contrasena character varying(20) NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) NOT NULL,
    fec_crea timestamp without time zone NOT NULL
);
    DROP TABLE public.sol_usuario;
       public         heap    postgres    false    210    3            �            1259    17568    ta_auditoria_id_auditoria_seq    SEQUENCE     �   CREATE SEQUENCE public.ta_auditoria_id_auditoria_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;
 4   DROP SEQUENCE public.ta_auditoria_id_auditoria_seq;
       public          postgres    false    3            �            1259    17570    ta_auditoria    TABLE     f  CREATE TABLE public.ta_auditoria (
    id_auditoria integer DEFAULT nextval('public.ta_auditoria_id_auditoria_seq'::regclass) NOT NULL,
    codigoerror integer NOT NULL,
    msjerror character varying(1000),
    metodo character varying(50),
    vigente boolean NOT NULL,
    usu_crea character varying(10),
    fec_crea timestamp with time zone NOT NULL
);
     DROP TABLE public.ta_auditoria;
       public         heap    postgres    false    218    3            f          0    17490    ma_color 
   TABLE DATA                 public          postgres    false    211   :t       g          0    17499 
   ma_estados 
   TABLE DATA                 public          postgres    false    212   Tt       h          0    17508    ma_rango_numero 
   TABLE DATA                 public          postgres    false    213   "u       i          0    17514    ma_tipo_apuesta 
   TABLE DATA                 public          postgres    false    214   �u       p          0    17606    sol_apuesta 
   TABLE DATA                 public          postgres    false    221   �v       j          0    17529    sol_credito 
   TABLE DATA                 public          postgres    false    215   �w       o          0    17593 
   sol_ruleta 
   TABLE DATA                 public          postgres    false    220   =x       k          0    17544 	   sol_token 
   TABLE DATA                 public          postgres    false    216   dy       l          0    17550    sol_usuario 
   TABLE DATA                 public          postgres    false    217   �z       n          0    17570    ta_auditoria 
   TABLE DATA                 public          postgres    false    219   D{       x           0    0    ma_color_id_color_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.ma_color_id_color_seq', 7, false);
          public          postgres    false    202            y           0    0    ma_estados_id_estado_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ma_estados_id_estado_seq', 7, false);
          public          postgres    false    203            z           0    0    ma_rango_numero_id_rango_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.ma_rango_numero_id_rango_seq', 7, false);
          public          postgres    false    204            {           0    0    ma_tipo_apuesta_id_tipo_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.ma_tipo_apuesta_id_tipo_seq', 7, false);
          public          postgres    false    205            |           0    0    sol_apuesta_id_apuesta_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.sol_apuesta_id_apuesta_seq', 14, true);
          public          postgres    false    206            }           0    0    sol_credito_id_credito_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.sol_credito_id_credito_seq', 7, false);
          public          postgres    false    207            ~           0    0    sol_ruleta_id_ruleta_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.sol_ruleta_id_ruleta_seq', 7, false);
          public          postgres    false    208                       0    0    sol_token_id_token_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.sol_token_id_token_seq', 13, true);
          public          postgres    false    209            �           0    0    sol_usuario_id_usuario_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.sol_usuario_id_usuario_seq', 7, true);
          public          postgres    false    210            �           0    0    ta_auditoria_id_auditoria_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.ta_auditoria_id_auditoria_seq', 7, false);
          public          postgres    false    218            �
           2606    17498    ma_color ma_color_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ma_color
    ADD CONSTRAINT ma_color_pkey PRIMARY KEY (id_color);
 @   ALTER TABLE ONLY public.ma_color DROP CONSTRAINT ma_color_pkey;
       public            postgres    false    211            �
           2606    17507    ma_estados ma_estados_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ma_estados
    ADD CONSTRAINT ma_estados_pkey PRIMARY KEY (id_estado);
 D   ALTER TABLE ONLY public.ma_estados DROP CONSTRAINT ma_estados_pkey;
       public            postgres    false    212            �
           2606    17513 $   ma_rango_numero ma_rango_numero_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.ma_rango_numero
    ADD CONSTRAINT ma_rango_numero_pkey PRIMARY KEY (id_rango);
 N   ALTER TABLE ONLY public.ma_rango_numero DROP CONSTRAINT ma_rango_numero_pkey;
       public            postgres    false    213            �
           2606    17522 $   ma_tipo_apuesta ma_tipo_apuesta_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.ma_tipo_apuesta
    ADD CONSTRAINT ma_tipo_apuesta_pkey PRIMARY KEY (id_tipo);
 N   ALTER TABLE ONLY public.ma_tipo_apuesta DROP CONSTRAINT ma_tipo_apuesta_pkey;
       public            postgres    false    214            �
           2606    17611    sol_apuesta sol_apuesta_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.sol_apuesta
    ADD CONSTRAINT sol_apuesta_pkey PRIMARY KEY (id_apuesta);
 F   ALTER TABLE ONLY public.sol_apuesta DROP CONSTRAINT sol_apuesta_pkey;
       public            postgres    false    221            �
           2606    17534    sol_credito sol_credito_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.sol_credito
    ADD CONSTRAINT sol_credito_pkey PRIMARY KEY (id_credito);
 F   ALTER TABLE ONLY public.sol_credito DROP CONSTRAINT sol_credito_pkey;
       public            postgres    false    215            �
           2606    17601    sol_ruleta sol_ruleta_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.sol_ruleta
    ADD CONSTRAINT sol_ruleta_pkey PRIMARY KEY (id_ruleta);
 D   ALTER TABLE ONLY public.sol_ruleta DROP CONSTRAINT sol_ruleta_pkey;
       public            postgres    false    220            �
           2606    17549    sol_token sol_token_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.sol_token
    ADD CONSTRAINT sol_token_pkey PRIMARY KEY (id_token);
 B   ALTER TABLE ONLY public.sol_token DROP CONSTRAINT sol_token_pkey;
       public            postgres    false    216            �
           2606    17555    sol_usuario sol_usuario_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.sol_usuario
    ADD CONSTRAINT sol_usuario_pkey PRIMARY KEY (id_usuario);
 F   ALTER TABLE ONLY public.sol_usuario DROP CONSTRAINT sol_usuario_pkey;
       public            postgres    false    217            �
           2606    17578    ta_auditoria ta_auditoria_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ta_auditoria
    ADD CONSTRAINT ta_auditoria_pkey PRIMARY KEY (id_auditoria);
 H   ALTER TABLE ONLY public.ta_auditoria DROP CONSTRAINT ta_auditoria_pkey;
       public            postgres    false    219            f   
   x���          g   �   x���A�@໿�ݶ`�����)Y]E�W,�����Ϣ�]�6s��|[d�������jܪ������D7�̡!����Zw}�����2Tq8�z�)�V�cV�Dp`�i:���a�����bȎ�(���D����,e�������#`Uk2�[�� �8D���y��wr`,���<�m��      h   �   x�5�A�0��⽩�W��Yv��A���b6e�S���'I�O^�٭B^TWL��Vm84�it?��Ҍ��k��+���&|T/�"	v�ukdC�d�����r�Jx��ў����4�Ko��L��� X*x�B���,v���8_��2�      i   �   x��ν�0�ᝫ��BR-���1$QWRK%M�6���\�1	�\�Nޜᩛ�:��nN��z�<y��I�:����A(�9`��Fj.Ո�)1:��[�q#�����ݟ�B���]��?��V�Fة ��4&$NK �5��<K�r��i��MP�WG'B�~=�Q?�
���d���qb\      p   �   x�͒Mk�0@�����Q&&jO=�A��^��t�F���o*]��voB�����LQ�'(�����x4}�N�S�����I7��vff�z�����,���v�nMo,���&��ԭվ�S�3����<���H��P���Q�˒�d�/3N���3C��f��#�9��B�#L��SPlV��b����12�ۼ�7[[��-�_K�fRn^����z�2Ic�f\neż��er�%�1�gJ!�3~ �g�      j   �   x�5��
�0 w��ۢ�H�V;up��j�R	�F���oR�pp��C{���+�~VrɭV�ňU:�\���3)m0�䭟��In���P�8ax��g<Η{;@�0T���][�aQpN9%�zN~h�"�YU��%�NI�|�>0�      o     x�͒���0�{�b:�X>9��)�+��6"� $��}�uH�j����~�aϧ�����!͏_0�K۔x�۳���
�4�S"��X�f(��C�ǩ�z���E�A��Ժ�4;�si�����[_5�Gp�[���N�l(?�C}��d�{��q��]�g���Ҁ����K.D""��������U����ًpLQ�X�C!W'8�\�d�c�ysr��o�9g�G$�<Z-[��Й4|͓D`IC&����	�8WR`*I�b��y��(\B      k   $  x���Mk�@�{~�ޢ���L���<��j{MS	#�����
��ri؅]&���K��z��Q�j����X���ۮ�,OjV���u=dз����D}U��ԕ��ɶh�]�>��r���ǧ��Z�l�bd�!����8Q�kz����M��0�.�RH)���2� ��Q��b'{����X��Z��Db?tL7dO�-"�%�@�/	3�� d��ّd���%������d�M���ȴ$RrS��`�_3Yb��f6�����zN��lIVj�Ȟm�akI ��Eߍ�g�      l   �   x�]ɽ�0�ᝫ�6 )��'�FЕ�ZIlI�~�F�缧i���C��GX�8�5�8���HܾF �c��/0%���K��)&.����>�9{+���p�;�J�I;>��k��r?�������f�3�ᄐ�@�
�lS�o�x���<�      n   
   x���         