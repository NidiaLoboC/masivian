-- Table: public.sol_token

-- DROP TABLE public.sol_token;

CREATE TABLE public.sol_token
(
    id_token integer NOT NULL DEFAULT nextval('sol_token_id_token_seq'::regclass),
    token character varying(100) COLLATE pg_catalog."default" NOT NULL,
    id_usuario integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_token_pkey PRIMARY KEY (id_token)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_token
    OWNER to postgres;