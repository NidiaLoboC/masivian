-- Table: public.sol_ruleta

-- DROP TABLE public.sol_ruleta;

CREATE TABLE public.sol_ruleta
(
    id_ruleta integer NOT NULL DEFAULT nextval('sol_ruleta_id_ruleta_seq'::regclass),
    descripcion character varying(500) COLLATE pg_catalog."default" NOT NULL,
    estado integer NOT NULL,
    numganador integer,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    usu_modif character varying(50) COLLATE pg_catalog."default" ,
    fec_modif timestamp with time zone ,
    CONSTRAINT sol_ruleta_pkey PRIMARY KEY (id_ruleta)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_ruleta
    OWNER to postgres;