-- Table: public.ma_rango_numero

-- DROP TABLE public.ma_rango_numero;

CREATE TABLE public.ma_rango_numero
(
    id_rango integer NOT NULL DEFAULT nextval('ma_rango_numero_id_rango_seq'::regclass),
    rango_ini integer NOT NULL,
	rango_fin integer NOT NULL,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ma_rango_numero_pkey PRIMARY KEY (id_rango)
)

TABLESPACE pg_default;

ALTER TABLE public.ma_rango_numero
    OWNER to postgres;