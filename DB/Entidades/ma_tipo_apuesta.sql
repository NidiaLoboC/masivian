-- Table: public.ma_tipo_apuesta

-- DROP TABLE public.ma_tipo_apuesta;

CREATE TABLE public.ma_tipo_apuesta
(
    id_tipo integer NOT NULL DEFAULT nextval('ma_tipo_apuesta_id_tipo_seq'::regclass),
    descripcion character varying(500) COLLATE pg_catalog."default" NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ma_tipo_apuesta_pkey PRIMARY KEY (id_tipo)
)

TABLESPACE pg_default;

ALTER TABLE public.ma_tipo_apuesta
    OWNER to postgres;