-- Table: public.ma_color

-- DROP TABLE public.ma_color;

CREATE TABLE public.ma_color
(
    id_color integer NOT NULL DEFAULT nextval('ma_color_id_color_seq'::regclass),
    descripcion character varying(500) COLLATE pg_catalog."default" NOT NULL,
	vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT ma_color_pkey PRIMARY KEY (id_color)
)

TABLESPACE pg_default;

ALTER TABLE public.ma_color
    OWNER to postgres;