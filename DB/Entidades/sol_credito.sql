-- Table: public.sol_credito

-- DROP TABLE public.sol_credito;

CREATE TABLE public.sol_credito
(
    id_credito integer NOT NULL DEFAULT nextval('sol_credito_id_credito_seq'::regclass),
    valor double precision NOT NULL,
    id_usuario integer NOT NULL,
    vigente boolean NOT NULL,
    usu_crea character varying(50) COLLATE pg_catalog."default" NOT NULL,
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_credito_pkey PRIMARY KEY (id_credito)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_credito
    OWNER to postgres;