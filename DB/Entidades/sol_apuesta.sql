-- Table: public.sol_apuesta

-- DROP TABLE public.sol_apuesta;

CREATE OR REPLACE TABLE public.sol_apuesta
(
    id_apuesta integer NOT NULL DEFAULT nextval('sol_apuesta_id_apuesta_seq'::regclass),
    apuesta double precision NOT NULL,
    id_usuario integer NOT NULL,
    id_ruleta integer NOT NULL,
    id_tipo integer NOT NULL,
    apuesta_numero integer,
    apuesta_color integer,
    vigente boolean NOT NULL,
    usu_crea character varying(10),
    fec_crea timestamp with time zone NOT NULL,
    CONSTRAINT sol_apuesta_pkey PRIMARY KEY (id_apuesta)
)

TABLESPACE pg_default;

ALTER TABLE public.sol_apuesta
    OWNER to postgres;