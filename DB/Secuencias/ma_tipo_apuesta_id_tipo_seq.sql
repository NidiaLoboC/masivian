-- SEQUENCE: public.ma_tipo_apuesta_id_tipo_seq

-- DROP SEQUENCE public.ma_tipo_apuesta_id_tipo_seqv;

CREATE SEQUENCE public.ma_tipo_apuesta_id_tipo_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.ma_tipo_apuesta_id_tipo_seq
    OWNER TO postgres;