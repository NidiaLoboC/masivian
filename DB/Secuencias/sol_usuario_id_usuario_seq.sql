-- SEQUENCE: public.sol_usuario_id_usuario_seq

-- DROP SEQUENCE public.sol_usuario_id_usuario_seq;

CREATE SEQUENCE public.sol_usuario_id_usuario_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.sol_usuario_id_usuario_seq
    OWNER TO postgres;