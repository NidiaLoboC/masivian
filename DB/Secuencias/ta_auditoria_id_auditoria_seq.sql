-- SEQUENCE: public.ta_auditoria_id_auditoria_seq

-- DROP SEQUENCE public.ta_auditoria_id_auditoria_seq;

CREATE SEQUENCE public.ta_auditoria_id_auditoria_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.ta_auditoria_id_auditoria_seq
    OWNER TO postgres;