-- SEQUENCE: public.ma_color_id_color_seq

-- DROP SEQUENCE public.ma_color_id_color_seq;

CREATE SEQUENCE public.ma_color_id_color_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.ma_color_id_color_seq
    OWNER TO postgres;