-- SEQUENCE: public.sol_token_id_token_seq

-- DROP SEQUENCE public.sol_token_id_token_seq;

CREATE SEQUENCE public.sol_token_id_token_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.sol_token_id_token_seq
    OWNER TO postgres;