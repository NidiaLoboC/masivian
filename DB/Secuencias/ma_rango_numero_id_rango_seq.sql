-- SEQUENCE: public.ma_rango_numero_id_rango_seq

-- DROP SEQUENCE public.ma_rango_numero_id_rango_seq;

CREATE SEQUENCE public.ma_rango_numero_id_rango_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.ma_rango_numero_id_rango_seq
    OWNER TO postgres;