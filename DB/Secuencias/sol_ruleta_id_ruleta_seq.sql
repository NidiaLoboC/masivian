-- SEQUENCE: public.sol_ruleta_id_ruleta_seq

-- DROP SEQUENCE public.sol_ruleta_id_ruleta_seq;

CREATE SEQUENCE public.sol_ruleta_id_ruleta_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.sol_ruleta_id_ruleta_seq
    OWNER TO postgres;