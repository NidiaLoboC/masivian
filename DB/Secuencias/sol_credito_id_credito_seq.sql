-- SEQUENCE: public.sol_credito_id_credito_seq

-- DROP SEQUENCE public.sol_credito_id_credito_seq;

CREATE SEQUENCE public.sol_credito_id_credito_seq
    INCREMENT 1
    START 7
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.sol_credito_id_credito_seq
    OWNER TO postgres;