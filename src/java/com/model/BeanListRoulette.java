package com.model;

/**
 *
 * @author 81001587
 */
public class BeanListRoulette {

    private int idRoulette;
    private String nameRoulette;
    private int idStatus;
    private String status;
    private String msgError;
    private boolean error;
    private int idUser;
    private String token;

    public BeanListRoulette() {
    }

    public BeanListRoulette(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public BeanListRoulette(int idUser, String token) {
        this.idUser = idUser;
        this.token = token;
    }

    public BeanListRoulette(int idRoulette, String nameRoulette, int idStatus, String status) {
        this.idRoulette = idRoulette;
        this.nameRoulette = nameRoulette;
        this.idStatus = idStatus;
        this.status = status;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(int idRoulette) {
        this.idRoulette = idRoulette;
    }

    public String getNameRoulette() {
        return nameRoulette;
    }

    public void setNameRoulette(String nameRoulette) {
        this.nameRoulette = nameRoulette;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
