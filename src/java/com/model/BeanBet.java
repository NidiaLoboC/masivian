package com.model;

/**
 *
 * @author 81001587
 */
public class BeanBet {

    private int idroulette;
    private double moneybet;
    private int typebet;
    private String numberbet;
    private String colorbet;
    private String token;
    private int iduser;
    private String msgError;
    private boolean error;

    public BeanBet() {
    }

    public BeanBet(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public BeanBet(int idroulette, double moneybet, int typebet, String numberbet, String colorbet, String token, int iduser) {
        this.idroulette = idroulette;
        this.moneybet = moneybet;
        this.typebet = typebet;
        this.numberbet = numberbet;
        this.colorbet = colorbet;
        this.token = token;
        this.iduser = iduser;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getIdroulette() {
        return idroulette;
    }

    public void setIdroulette(int idroulette) {
        this.idroulette = idroulette;
    }

    public double getMoneybet() {
        return moneybet;
    }

    public void setMoneybet(double moneybet) {
        this.moneybet = moneybet;
    }

    public int getTypebet() {
        return typebet;
    }

    public void setTypebet(int typebet) {
        this.typebet = typebet;
    }

    public String getNumberbet() {
        return numberbet;
    }

    public void setNumberbet(String numberbet) {
        this.numberbet = numberbet;
    }

    public String getColorbet() {
        return colorbet;
    }

    public void setColorbet(String colorbet) {
        this.colorbet = colorbet;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

}
