package com.model;

import java.util.List;

/**
 *
 * @author 81001587
 */
public class BeanSelectWinner {

    private int idRoulette;
    private int idUser;
    private String token;
    private int status;
    private String msgError;
    private boolean error;
    private String nameUser;
    private String nameRoulette;
    private int winningNumber;
    private int numberBet;
    private double moneyBet;
    private String colorBet;
    private double winnedMoney;
    private String ganador;
    private List<BeanSelectWinner> bsw;

    public BeanSelectWinner() {
    }

    public BeanSelectWinner(String msgError, boolean error) {
        this.msgError = msgError;
        this.error = error;
    }

    public BeanSelectWinner(int idRoulette, int idUser, String token) {
        this.idRoulette = idRoulette;
        this.idUser = idUser;
        this.token = token;
    }

    public BeanSelectWinner(int idRoulette, String nameRoulette, int winningNumber, List<BeanSelectWinner> bsw) {
        this.idRoulette = idRoulette;
        this.nameRoulette = nameRoulette;
        this.winningNumber = winningNumber;
        this.bsw = bsw;
    }

    public BeanSelectWinner(int idUser, String nameUser,int numberBet, double moneyBet, String colorBet, double winnedMoney, String ganador) {
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.numberBet = numberBet;
        this.moneyBet = moneyBet;
        this.colorBet = colorBet;
        this.winnedMoney = winnedMoney;
        this.ganador = ganador;
    }

    public int getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(int idRoulette) {
        this.idRoulette = idRoulette;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getNameRoulette() {
        return nameRoulette;
    }

    public void setNameRoulette(String nameRoulette) {
        this.nameRoulette = nameRoulette;
    }

    public int getWinningNumber() {
        return winningNumber;
    }

    public void setWinningNumber(int winningNumber) {
        this.winningNumber = winningNumber;
    }

    public int getNumberBet() {
        return numberBet;
    }

    public void setNumberBet(int numberBet) {
        this.numberBet = numberBet;
    }

    public double getMoneyBet() {
        return moneyBet;
    }

    public void setMoneyBet(double moneyBet) {
        this.moneyBet = moneyBet;
    }

    public String getColorBet() {
        return colorBet;
    }

    public void setColorBet(String colorBet) {
        this.colorBet = colorBet;
    }

    public double getWinnedMoney() {
        return winnedMoney;
    }

    public void setWinnedMoney(double winnedMoney) {
        this.winnedMoney = winnedMoney;
    }

    public String getGanador() {
        return ganador;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }

    public List<BeanSelectWinner> getBsw() {
        return bsw;
    }

    public void setBsw(List<BeanSelectWinner> bsw) {
        this.bsw = bsw;
    }

}
