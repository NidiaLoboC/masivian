/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author 81001587
 */
public class BeanRoulette {

    private int idRoulette;
    private int idUser;
    private String token;
    private String msgError;
    private boolean error;

    public BeanRoulette() {
    }

    public BeanRoulette(int idUser, String token) {
        this.idUser = idUser;
        this.token = token;
    }

    public BeanRoulette(int idRoulette, String msgError, boolean error) {
        this.idRoulette = idRoulette;
        this.msgError = msgError;
        this.error = error;
    }

    public BeanRoulette(int idRoulette, int idUser, String token) {
        this.idRoulette = idRoulette;
        this.idUser = idUser;
        this.token = token;
    }

    public BeanRoulette(int idRoulette, int idUser, String token, String msgError, boolean error) {
        this.idRoulette = idRoulette;
        this.idUser = idUser;
        this.token = token;
        this.msgError = msgError;
        this.error = error;
    }

    public int getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(int idRoulette) {
        this.idRoulette = idRoulette;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
