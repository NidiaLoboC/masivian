package com.model;

import java.util.List;

/**
 *
 * @author 81001587
 */
public class BeanClosedRoulette {

    private  int idRoulette;
    private  String nameRoulette;
    private  int winningNumber;
    private  List<BeanSelectWinner> bsw;
    public BeanClosedRoulette(int idRoulette, String nameRoulette, int winningNumber, List<BeanSelectWinner> bsw) {
        this.idRoulette = idRoulette;
        this.nameRoulette = nameRoulette;
        this.winningNumber = winningNumber;
        this.bsw = bsw;
    }

    public int getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(int idRoulette) {
        this.idRoulette = idRoulette;
    }

    public String getNameRoulette() {
        return nameRoulette;
    }

    public void setNameRoulette(String nameRoulette) {
        this.nameRoulette = nameRoulette;
    }

    public int getWinningNumber() {
        return winningNumber;
    }

    public void setWinningNumber(int winningNumber) {
        this.winningNumber = winningNumber;
    }

    public List<BeanSelectWinner> getBsw() {
        return bsw;
    }

    public void setBsw(List<BeanSelectWinner> bsw) {
        this.bsw = bsw;
    }

}
