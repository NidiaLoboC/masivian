package com.manage;

import com.model.BeanBet;
import com.model.BeanRoulette;
import com.model.BeanSelectWinner;
import com.model.BeanSession;

/**
 *
 * @author 81001587
 */
public class Exceptions {

    public Exceptions() {
    }

    /**
     * ProcessError
     *
     * @param endPointError 1=login 2=createRoulette 3=startroulette 4=typebet
     * 5=bet 6=winner 7=lisroulette 8=logout
     * @return
     */
    protected Object ProcessError(int endPointError) {
        String msj = null;
        Object obj = new Object();
        switch (endPointError) {
            case 1:
                msj = "No se ha podido iniciar sesión, por favor revise.";
                obj = new BeanSession(0, "", msj, true);
                break;
            case 2:
                msj = "No se ha podido crear ruleta, por favor revise.";
                obj = new BeanRoulette(0, msj, true);
                break;
            case 3:
                msj = "No se ha podido iniciar ruleta, por favor revise.";
                obj = new BeanRoulette(0, msj, true);
                break;
            case 4:
                msj = "Apuesta realizada no valida, revisar el número o color digitado.";
                obj = new BeanBet(msj, true);
                break;
            case 5:
                msj = "No fue registrada con exito la apuesta.";
                obj = new BeanBet(msj, true);
                break;
            case 6:
                msj = "No fue posible elegir un ganador, intente mas tarde.";
                obj = new BeanSelectWinner(msj, true);
                break;
            case 7:
                msj = "No fue posible listar las ruletas, intente mas tarde.";
                obj = new BeanSelectWinner(msj, true);
                break;
            case 8:
                msj = "No se ha podido cerrar sesión.";
                obj = new BeanSession(msj, true);
                break;

        }
        return obj;
    }

}
