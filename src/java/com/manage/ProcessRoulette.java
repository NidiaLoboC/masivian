package com.manage;

import com.database.CallProcedures;
import com.database.Conex;
import com.google.gson.Gson;
import com.model.BeanBet;
import com.model.BeanListRoulette;
import com.model.BeanRoulette;
import com.model.BeanSelectWinner;
import com.model.BeanSession;
import java.sql.SQLException;

/**
 *
 * @author 81001587
 */
public class ProcessRoulette {

    private CallProcedures callPro = new CallProcedures();
    private FunctionsRoulette funtions = new FunctionsRoulette();
    private Gson gson = new Gson();
    private boolean error = true;
    private Exceptions exc = new Exceptions();
    private Audit aud = new Audit();
    private String nameMethod, nameClass;

    public String GetTokenSession(BeanSession bs) {
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {
            return gson.toJson(callPro.procCreateToken(con.conection(), bs));
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
            return gson.toJson(exc.ProcessError(1));
        }
    }

    public String CreateRoulette(BeanRoulette br) {
        int state = 1;//created
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {
            return gson.toJson(callPro.procInsertUpdateRoulette(con.conection(), br, state));
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
            return gson.toJson(exc.ProcessError(2));
        }
    }

    public String InitializeRoulette(BeanRoulette br) {
        int state = 2;//open
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {
            return gson.toJson(callPro.procInsertUpdateRoulette(con.conection(), br, state));
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
            return gson.toJson(exc.ProcessError(2));
        }
    }

    public String DoBet(BeanBet bb, boolean validBet) {
        if (validBet) {
            nameMethod = new Object() {
            }.getClass().getEnclosingMethod().getName();
            nameClass = this.getClass().getSimpleName();
            Conex con = new Conex();
            try {
                return gson.toJson(callPro.procCreateBet(con.conection(), bb));
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
                return gson.toJson(exc.ProcessError(5));
            }
        } else {
            return gson.toJson(exc.ProcessError(4));
        }
    }

    public String ChooseWinner(BeanSelectWinner bsw) {
        int state = 3;//closed
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();

        return gson.toJson(funtions.WinnerRoulette(con.conection(), bsw, state));
    }

    public String RouletteDetail(BeanListRoulette blr) {
        Conex con = new Conex();

        return gson.toJson(funtions.RouletteListDetail(con.conection(), blr));
    }

    public String OutTokenSession(BeanSession bs) {
        nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName();
        nameClass = this.getClass().getSimpleName();
        Conex con = new Conex();
        try {
            return gson.toJson(callPro.procInvalidateToken(con.conection(), bs));
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);

            return gson.toJson(exc.ProcessError(8));
        }
    }

}
