package com.manage;

import com.model.BeanBet;
import com.model.BeanListRoulette;
import com.model.BeanRoulette;
import com.model.BeanSelectWinner;
import com.model.BeanSession;

/**
 *
 * @author 81001587
 */
public class Controller {

    ProcessRoulette procR = new ProcessRoulette();

    public String GetSession(BeanSession bs) {
        return procR.GetTokenSession(bs);
    }

    public String OpenRoulette(BeanRoulette br) {
        return procR.CreateRoulette(br);
    }

    public String StartRoulette(BeanRoulette br) {
        return procR.InitializeRoulette(br);
    }

    public String MakeBet(BeanBet bb, boolean validBet) {
        return procR.DoBet(bb, validBet);
    }

    public String CloseRoulette(BeanSelectWinner bsw) {
        return procR.ChooseWinner(bsw);
    }

    public String ListRoulette(BeanListRoulette blr) {
        return procR.RouletteDetail(blr);
    }

    public String OutSession(BeanSession bs) {
        return procR.GetTokenSession(bs);
    }

}
