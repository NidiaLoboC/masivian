package com.manage;

import com.google.gson.Gson;
import com.model.BeanClosedRoulette;
import com.model.BeanListRoulette;
import com.model.BeanSelectWinner;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 81001587
 */
public class FunctionsRoulette {

    private Audit aud = new Audit();
    private Exceptions exc = new Exceptions();
    private Gson gson = new Gson();

    public FunctionsRoulette() {
    }

    public Object WinnerRoulette(Connection con, BeanSelectWinner bsw, int status) {
        List<BeanSelectWinner> listbsw = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String nameRoulete = null;
        int number = 0, idRoulette = 0;
        String sql = "SELECT * FROM public.fr_cerrar_ruleta(?,?,?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, bsw.getToken());
            pr.setInt(2, bsw.getIdUser());
            pr.setInt(3, status);
            pr.setInt(4, bsw.getIdRoulette());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    System.out.println(rs.getStatement());
                    listbsw.add(new BeanSelectWinner(rs.getInt("ID_USUARIO"), rs.getString("NOMBRE"), rs.getInt("NUMERO_APOSTADO"),
                            rs.getDouble("APUESTA"), rs.getString("COLOR_APOSTADO"), rs.getDouble("VALORGANADO"), rs.getString("GANADOR")));
                    idRoulette = rs.getInt("ID_RULETA");
                    nameRoulete = rs.getString("NOMBRE_RULETA");
                    number = rs.getInt("NUMGANADOR");
                }
                rs.close();
                return new BeanClosedRoulette(idRoulette, nameRoulete, number, listbsw);
            } else {
                return exc.ProcessError(6);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(6);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }

    public Object RouletteListDetail(Connection con, BeanListRoulette blr) {
        List<BeanListRoulette> listblr = new ArrayList<>();
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        String sql = "SELECT * FROM public.fr_listar_ruleta(?,?)";
        try {
            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1, blr.getToken());
            pr.setInt(2, blr.getIdUser());
            ResultSet rs = (ResultSet) pr.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    System.out.println(rs.getStatement());
                    listblr.add(new BeanListRoulette(rs.getInt("ID_RULETA"), rs.getString("NOMBRERULETA"), rs.getInt("ID_ESTADO"),
                            rs.getString("DESCRIPCION")));
                }
                rs.close();

                return listblr;
            } else {
                return exc.ProcessError(6);
            }
        } catch (SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
            return exc.ProcessError(6);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
                aud.SaveAudit(ex.getErrorCode(), message, nameMethod, nameClass);
            }
        }
    }
}
