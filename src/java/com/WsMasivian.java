/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

import com.manage.Controller;
import com.model.BeanBet;
import com.model.BeanListRoulette;
import com.model.BeanRoulette;
import com.model.BeanSelectWinner;
import com.model.BeanSession;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author 81001587
 */
@Path("")
public class WsMasivian {

    private Controller controll = new Controller();

    public WsMasivian() {
    }

    @POST
    @Path("roulette/login")
    @Produces("application/json")
    public Response RouletteLogin(@FormParam("user") @DefaultValue("") String user,
            @FormParam("pass") @DefaultValue("") String pass) {
        if (user.length() > 0 && pass.length() > 0) {
            BeanSession bs = new BeanSession(user, pass);

            return Response.ok(controll.GetSession(bs))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("roulette/opening")
    @Produces("application/json")
    public Response RouletteOpening(@Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();

        if (headerParams.get("iduser") != null && headerParams.get("token") != null) {
            BeanRoulette br = new BeanRoulette(Integer.parseInt(headerParams.get("iduser").get(0)), headerParams.get("token").get(0));

            return Response.ok(controll.OpenRoulette(br))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("roulette/start")
    @Produces("application/json")
    public Response RouletteStart(@FormParam("idroulette") int idRoulette, @Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();

        if (headerParams.get("iduser") != null && headerParams.get("token") != null && idRoulette > 0) {
            BeanRoulette br = new BeanRoulette(idRoulette, Integer.parseInt(headerParams.get("iduser").get(0)),
                    headerParams.get("token").get(0));

            return Response.ok(controll.StartRoulette(br))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("roulette/bet")
    @Produces("application/json")
    public Response RouletteBet(@FormParam("idroulette") int idroulette, @FormParam("moneybet") double moneybet,
            @FormParam("typebet") int typebet, @FormParam("numberbet") @DefaultValue("") String numberbet,
            @FormParam("colorbet") @DefaultValue("") String colorbet, @Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
        boolean validBet = (((numberbet.length() == 0 || colorbet.length() > 0) || (numberbet.length() > 0 || colorbet.length() == 0)));
        if (headerParams.get("iduser") != null && headerParams.get("token") != null && idroulette > 0 && (moneybet > 0 || moneybet <= 10000)
                && typebet > 0) {
            BeanBet bb = new BeanBet(idroulette, moneybet, typebet, numberbet, colorbet, headerParams.get("token").get(0), Integer.parseInt(headerParams.get("iduser").get(0)));

            return Response.ok(controll.MakeBet(bb, validBet))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("roulette/closing")
    @Produces("application/json")
    public Response RouletteClosing(@FormParam("idroulette") int idroulette, @Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();

        if (headerParams.get("iduser") != null && headerParams.get("token") != null && idroulette > 0) {
            BeanSelectWinner bsw = new BeanSelectWinner(idroulette, Integer.parseInt(headerParams.get("iduser").get(0)),
                    headerParams.get("token").get(0));

            return Response.ok(controll.CloseRoulette(bsw))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("roulette/list")
    @Produces("application/json")
    public Response RouletteList(@Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
        if (headerParams.get("iduser") != null && headerParams.get("token") != null) {
            BeanListRoulette blr = new BeanListRoulette(Integer.parseInt(headerParams.get("iduser").get(0)),
                    headerParams.get("token").get(0));

            return Response.ok(controll.ListRoulette(blr))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }

    @POST
    @Path("roulette/logout")
    @Produces("application/json")
    public Response RouletteLogout(@Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();

        if (headerParams.get("iduser") != null && headerParams.get("token") != null) {
            BeanSession bs = new BeanSession(Integer.parseInt(headerParams.get("iduser").get(0)), headerParams.get("token").get(0));

            return Response.ok(controll.OutSession(bs))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                    .header("Access-Control-Max-Age", "3600")
                    .header("Access-Control-Allow-Headers", "x-requested-with").build();
        }
    }
}
