package com.database;

import com.manage.Audit;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 81001587
 */
public class Conex {

    private Audit aud = new Audit();
    private Connection conectionDB = null;
    private String setting = "Setting.properties";
    public Conex() {
    }

    public Connection conection() {
        String nameMethod = new Object() {
        }.getClass().getEnclosingMethod().getName(),
                nameClass = this.getClass().getSimpleName();
        try {
            Properties prop = new Properties();
            prop.load(new FileReader(this.setting));
            Class.forName("org.postgresql.Driver");
            this.conectionDB = DriverManager.getConnection("jdbc:postgresql://" + prop.getProperty("server") + ":" + prop.getProperty("port") + "/" + prop.getProperty("database") + "", prop.getProperty("user"), prop.getProperty("password"));

            return this.conectionDB;
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            String message = (ex.getMessage().length() > 1000) ? ex.getMessage().substring(0, 1000) : ex.getMessage();
            aud.SaveAudit(0, message, nameMethod, nameClass);
        }
        return this.conectionDB;
    }
}
