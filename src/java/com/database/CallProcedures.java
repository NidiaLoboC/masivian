package com.database;

import com.model.BeanBet;
import com.model.BeanRoulette;
import com.model.BeanSession;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 81001587
 */
public class CallProcedures {

    private boolean error;
    private String msgError;

    public List<BeanSession> procCreateToken(Connection con, BeanSession bs) throws SQLException {
        List<BeanSession> bsession = new ArrayList<>();
        String token;
        int iduser;
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_login(?,?,?,?,?,?)")) {
            cs.setString(1, bs.getUser());
            cs.setString(2, bs.getPass());
            cs.setNull(3, java.sql.Types.INTEGER);
            cs.setNull(4, java.sql.Types.VARCHAR);
            cs.setNull(5, java.sql.Types.VARCHAR);
            cs.setNull(6, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(3, Types.INTEGER);
            cs.registerOutParameter(4, Types.VARCHAR);
            cs.registerOutParameter(5, Types.VARCHAR);
            cs.registerOutParameter(6, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(6);
            this.msgError = cs.getString(5);
            if (!error) {
                iduser = cs.getInt(3);
                token = cs.getString(4);
                bsession.add(new BeanSession(iduser, token, this.msgError, error));
                con.commit();
            } else {
                bsession.add(new BeanSession(0, "", this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return bsession;
    }

    public List<BeanRoulette> procInsertUpdateRoulette(Connection con, BeanRoulette br, int state) throws SQLException {
        List<BeanRoulette> bRoulette = new ArrayList<>();
        int idRoulette;
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_crear_actualiza_ruleta(?,?,?,?,?,?,?)")) {
            cs.setString(1, br.getToken());
            cs.setInt(2, br.getIdUser());
            cs.setInt(3, state);
            if (state == 1) {
                cs.setInt(4, 0);
            } else {
                cs.setInt(4, br.getIdRoulette());
            }
            cs.setNull(5, java.sql.Types.INTEGER);
            cs.setNull(6, java.sql.Types.VARCHAR);
            cs.setNull(7, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(4, Types.INTEGER);
            cs.registerOutParameter(6, Types.VARCHAR);
            cs.registerOutParameter(7, Types.BOOLEAN);
            cs.executeUpdate();
            this.msgError = cs.getString(6);
            this.error = cs.getBoolean(7);
            if (!error) {
                idRoulette = cs.getInt(4);
                bRoulette.add(new BeanRoulette(idRoulette, this.msgError, error));
                con.commit();
            } else {
                bRoulette.add(new BeanRoulette(0, this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return bRoulette;
    }

    public List<BeanBet> procCreateBet(Connection con, BeanBet bb) throws SQLException {
        List<BeanBet> BeanBet = new ArrayList<>();
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_realizar_apuesta(?,?,?,?,?,?,?,?,?)")) {
            cs.setString(1, bb.getToken());
            cs.setInt(2, bb.getIduser());
            if (bb.getNumberbet().length() > 0) {
                cs.setInt(3, Integer.parseInt(bb.getNumberbet()));
            } else {
                cs.setNull(4, java.sql.Types.INTEGER);
            }
            if (bb.getColorbet().length() > 0) {
                cs.setInt(4, Integer.parseInt(bb.getColorbet()));
            } else {
                cs.setNull(4, java.sql.Types.INTEGER);
            }
            cs.setDouble(5, bb.getMoneybet());
            cs.setInt(6, bb.getIdroulette());
            cs.setInt(7, bb.getTypebet());
            cs.setNull(8, java.sql.Types.VARCHAR);
            cs.setNull(9, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(8, Types.VARCHAR);
            cs.registerOutParameter(9, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(9);
            this.msgError = cs.getString(8);
            if (!error) {
                BeanBet.add(new BeanBet(this.msgError, error));
                con.commit();
            } else {
                BeanBet.add(new BeanBet(this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return BeanBet;
    }

    public List<BeanSession> procInvalidateToken(Connection con, BeanSession bs) throws SQLException {
        List<BeanSession> bsession = new ArrayList<>();
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_logout(?,?,?,?)")) {
            cs.setInt(1, bs.getIdUser());
            cs.setString(2, bs.getToken());
            cs.setNull(3, java.sql.Types.VARCHAR);
            cs.setNull(4, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(3, Types.VARCHAR);
            cs.registerOutParameter(4, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(4);
            this.msgError = cs.getString(3);
            if (!error) {
                bsession.add(new BeanSession(this.msgError, error));
                con.commit();
            } else {
                bsession.add(new BeanSession(this.msgError, this.error));
                con.rollback();
            }
        }
        con.close();

        return bsession;
    }

    public void procLoggerRecord(Connection con, int errorCode, String msjError,
            String methodError, String classError) throws SQLException {
        con.setAutoCommit(false);
        try (CallableStatement cs = con.prepareCall("CALL public.pr_logout(?,?,?,?)")) {
            cs.setInt(1, errorCode);
            cs.setString(2, msjError);
            cs.setString(3, methodError);
            cs.setNull(4, java.sql.Types.VARCHAR);
            cs.setNull(5, java.sql.Types.BOOLEAN);
            cs.registerOutParameter(4, Types.VARCHAR);
            cs.registerOutParameter(5, Types.BOOLEAN);
            cs.executeUpdate();
            this.error = cs.getBoolean(5);
            this.msgError = cs.getString(4);
            if (!error) {
                con.commit();
            } else {
                con.rollback();
            }
        }
        con.close();
    }
}
